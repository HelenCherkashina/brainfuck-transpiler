package com.brainfuck.transpiler.transpilers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static junit.framework.TestCase.assertEquals;

@RunWith(Parameterized.class)
public class JsCodeTranspilerTest {
    private Transpiler transpiler;

    private String expression;
    private String expectedCode;

    public JsCodeTranspilerTest(String expression, String expectedCode) {
        this.expression = expression;
        this.expectedCode = expectedCode;
    }

    @Before
    public void setUp() {
        this.transpiler = new JsCodeTranspiler();
    }

    @Test
    public void testPrimeNumberChecker() {
        assertEquals(expectedCode, transpiler.transpile(singletonList(expression)));
    }


    @Parameterized.Parameters
    public static List primeNumbers() {
        return asList(new Object[][]{
                {"+++++>+++[-<->]<.", loadFile("transpiledcode/case1.js")},
                {"+++++[->+++<]>.", loadFile("transpiledcode/case2.js")},
                {"++++++++++++[--->+<]>..", loadFile("transpiledcode/case3.js")},
                {",[.-[->+<\n]>+]", loadFile("transpiledcode/case4.js")},
                {"+++++[->+[+]+<]>.", loadFile("transpiledcode/case5.js")}
        });
    }

    private static String loadFile(String fileName) {
        ClassLoader classLoader = JsCodeTranspilerTest.class.getClassLoader();
        return new BufferedReader(new InputStreamReader(classLoader.getResourceAsStream(fileName))).lines()
                .parallel().collect(Collectors.joining("\n"));
    }
}