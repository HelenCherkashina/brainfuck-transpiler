package com.brainfuck.transpiler.verifiers;

import com.brainfuck.transpiler.exceptions.CompilationException;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BrainfuckCodeVerifierTest {
    private Verifier verifier;

    @Before
    public void setUp() {
        verifier = new BrainfuckCodeVerifier();
    }

    @Test
    public void shouldVerifyCodeSuccessfully() {
        //given
        List<String> code = Arrays.asList("+>>>[>]>>", ">.-->+.", ">.\t+++><\n++,.");

        //when
        verifier.verify(code);

        //then
    }

    @Test
    public void shouldThrowCompilationErrorIfIllegalCharactersAreFound() {
        //given
        List<String> code = Arrays.asList("+>>>[>]>>", ">.-*>+.", ">.\t++><\n++,.");

        //when
        //then
        try {
            verifier.verify(code);
        } catch (CompilationException e) {
            assertEquals("Compilation error(2:4). Illegal character *", e.getMessage());
        }
    }

    @Test
    public void shouldThrowCompilationErrorIfUnexpectedOpeningLoopBracketIsFound() {
        //given
        List<String> code = Arrays.asList("+>>>[>]>>", ">.->+.", ">.\t++[><\n++,.");

        //when
        //then
        try {
            verifier.verify(code);
        } catch (CompilationException e) {
            assertEquals("Unexpected opening loop bracket.", e.getMessage());
        }
    }

    @Test
    public void shouldThrowCompilationErrorIfUnexpectedClosingLoopBracketIsFound() {
        //given
        List<String> code = Arrays.asList("+>>>[>]>>", ">.->+.", ">.\t++[>]]<\n++,.");

        //when
        //then
        try {
            verifier.verify(code);
        } catch (CompilationException e) {
            assertEquals("Compilation error(3:9). Unexpected closing loop bracket.", e.getMessage());
        }
    }
}