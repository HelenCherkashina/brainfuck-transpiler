package com.brainfuck.transpiler.verifiers;

import com.brainfuck.transpiler.exceptions.CompilationException;

import java.util.Arrays;
import java.util.List;

import static com.brainfuck.transpiler.BrainfuckInstructionType.END_LOOP_BRACKET;
import static com.brainfuck.transpiler.BrainfuckInstructionType.START_LOOP_BRACKET;

public class BrainfuckCodeVerifier implements Verifier {
    private static final List<Character> LEGAL_CHARACTERS =
            Arrays.asList('+', '-', '<', '>', '.', ',', '[', ']', '\t', '\n');

    @Override
    public void verify(List<String> code) {
        for (String lineOfCode : code) {
            char[] codeArray = lineOfCode.toCharArray();

            int loopBracketsCounts = 0;

            for (int index = 0; index < codeArray.length; index++) {
                if (!LEGAL_CHARACTERS.contains(codeArray[index])) {
                    throw new CompilationException(code.indexOf(lineOfCode) + 1, index + 1, "Illegal character " + codeArray[index]);
                }

                if (codeArray[index] == START_LOOP_BRACKET.getSymbol()) {
                    loopBracketsCounts++;
                }

                if (codeArray[index] == END_LOOP_BRACKET.getSymbol()) {
                    if (loopBracketsCounts == 0) {
                        throw new CompilationException(code.indexOf(lineOfCode) + 1, index + 1, "Unexpected closing " +
                                "loop bracket.");
                    }
                    loopBracketsCounts--;
                }
            }

            if (loopBracketsCounts > 0) {
                throw new CompilationException("Unexpected opening loop bracket.");
            }
        }
    }
}