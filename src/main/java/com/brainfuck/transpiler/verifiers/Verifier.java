package com.brainfuck.transpiler.verifiers;

import java.util.List;

public interface Verifier {
    void verify(List<String> code);
}