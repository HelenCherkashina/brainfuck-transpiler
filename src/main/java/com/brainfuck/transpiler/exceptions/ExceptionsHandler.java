package com.brainfuck.transpiler.exceptions;

import org.apache.log4j.Logger;

public class ExceptionsHandler implements Thread.UncaughtExceptionHandler {
    private static Logger LOGGER = Logger.getLogger(ExceptionsHandler.class);

    public void uncaughtException(Thread t, Throwable e) {
        LOGGER.error(e.getMessage());
        LOGGER.error("==========BUILD FAILED==========");
    }
}
