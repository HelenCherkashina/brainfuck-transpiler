package com.brainfuck.transpiler.exceptions;

public class SourceFileException extends RuntimeException {
    public SourceFileException(String message) {
        super(message);
    }
}