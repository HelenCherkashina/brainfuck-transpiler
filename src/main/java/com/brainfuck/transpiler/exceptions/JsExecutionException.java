package com.brainfuck.transpiler.exceptions;

public class JsExecutionException extends RuntimeException {
    public JsExecutionException() {
        super("Can't execute transpiled javascript code");
    }
}
