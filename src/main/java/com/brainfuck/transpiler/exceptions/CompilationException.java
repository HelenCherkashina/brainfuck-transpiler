package com.brainfuck.transpiler.exceptions;

public class CompilationException extends RuntimeException {
    private static final String COMPILATION_DEFAULT_MESSAGE = "Compilation error(%s:%s). %s";

    public CompilationException(String message) {
        super(message);
    }

    public CompilationException(int lineNumber, int columnNumber, String message) {
        super(String.format(COMPILATION_DEFAULT_MESSAGE, lineNumber, columnNumber, message));
    }
}