package com.brainfuck.transpiler.transpilers;

import java.util.List;

public interface Transpiler {
    String transpile(List<String> linesOfCode);
}