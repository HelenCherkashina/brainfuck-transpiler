package com.brainfuck.transpiler.transpilers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.brainfuck.transpiler.BrainfuckInstructionType.valueOf;
import static java.lang.String.join;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.apache.commons.lang3.StringUtils.replaceEach;

public class JsCodeTranspiler implements Transpiler {
    private static final Pattern LOGICAL_INSTRUCTIONS_PATTERN = Pattern.compile("(\\++|-+|>+|<+|\\.|,+|\\[|\\])");
    private static final String DEFAULT_INITIALIZATION_VARIABLES = "(function() { var array = []; var index = 0; var console = {}; console.log = print;";
    private static final String DEFAULT_CLOSING_SCRIPT = "})();";

    @Override
    public String transpile(List<String> linesOfCode) {
        return DEFAULT_INITIALIZATION_VARIABLES + transpileCode(preprocessCode(linesOfCode));
    }

    private String transpileCode(String code) {
        List<String> instructions = getCodeInstructions(code);
        String transpiledCode = instructions.stream()
                .map(instruction -> String.format(valueOf(instruction.charAt(0)).getTranspiledExpression(), instruction.length()))
                .collect(Collectors.joining(EMPTY));
        return transpiledCode + DEFAULT_CLOSING_SCRIPT;
    }

    private String preprocessCode(List<String> linesOfCode) {
        return replaceEach(join(EMPTY, linesOfCode), new String[]{"\t", "\n", ","}, new String[]{EMPTY, EMPTY, EMPTY});
    }

    private List<String> getCodeInstructions(String code) {
        List<String> logicalInstructions = new ArrayList<>();

        Matcher matcher = LOGICAL_INSTRUCTIONS_PATTERN.matcher(code);

        while (matcher.find()) {
            logicalInstructions.add(matcher.group());
        }

        return logicalInstructions;
    }
}