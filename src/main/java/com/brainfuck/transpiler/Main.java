package com.brainfuck.transpiler;

import com.brainfuck.transpiler.exceptions.ExceptionsHandler;
import com.brainfuck.transpiler.exceptions.SourceFileException;
import com.brainfuck.transpiler.executors.Executor;
import com.brainfuck.transpiler.executors.JSExecutor;
import com.brainfuck.transpiler.transpilers.JsCodeTranspiler;
import com.brainfuck.transpiler.transpilers.Transpiler;
import com.brainfuck.transpiler.verifiers.BrainfuckCodeVerifier;
import com.brainfuck.transpiler.verifiers.Verifier;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.lang3.StringUtils.substringBeforeLast;

public class Main {
    private static final Logger LOGGER = Logger.getLogger(Main.class);
    private static final Thread.UncaughtExceptionHandler handler = new ExceptionsHandler();

    static {
        Thread.setDefaultUncaughtExceptionHandler(handler);
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            throw new SourceFileException("Source file argument is missing");
        }

        Path path = Paths.get(args[0]);

        try (Stream<String> stream = Files.lines(path)) {
            List<String> linesOfCode = stream.collect(Collectors.toList());

            if (linesOfCode.isEmpty()) {
                throw new SourceFileException("Source file is empty");
            }

            LOGGER.info("Code verification");
            Verifier verifier = new BrainfuckCodeVerifier();
            verifier.verify(linesOfCode);

            LOGGER.info("Code transpilation");
            Transpiler transpiler = new JsCodeTranspiler();
            String jsTranspiledCode = transpiler.transpile(linesOfCode);

            String fileName = substringBeforeLast(path.toAbsolutePath().toString(), ".") + ".js";
            LOGGER.info("Write transpiled code to file " + fileName);
            Files.write(Paths.get(fileName), jsTranspiledCode.getBytes());

            LOGGER.info("Code execution");
            Executor executor = new JSExecutor();
            executor.execute(jsTranspiledCode);
        } catch (IOException e) {
            throw new SourceFileException("Can't open file " + args[0]);
        }
    }
}