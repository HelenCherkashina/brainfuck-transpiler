package com.brainfuck.transpiler;

import java.util.Arrays;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public enum BrainfuckInstructionType {
    MOVE_TO_NEXT('>', "index += %s;"),
    MOVE_TO_PREVIOUS('<', "index -= %s;"),
    PLUS('+', "array[index] = (array[index] || 0) + %s;"),
    MINUS('-', "array[index] = (array[index] || 0) - %s;"),
    DOT('.', "console.log(array[index]);"),
    COMMA(',', EMPTY),
    START_LOOP_BRACKET('[', "while(array[index] !== 0) {"),
    END_LOOP_BRACKET(']', "}");

    private char symbol;
    private String transpiledExpression;

    BrainfuckInstructionType(char symbol, String transpiledExpression) {
        this.symbol = symbol;
        this.transpiledExpression = transpiledExpression;
    }

    public char getSymbol() {
        return symbol;
    }

    public String getTranspiledExpression() {
        return transpiledExpression;
    }

    public static BrainfuckInstructionType valueOf(char symbol) {
        return Arrays.stream(values())
                .filter(type -> symbol == type.symbol)
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
