package com.brainfuck.transpiler.executors;

import com.brainfuck.transpiler.exceptions.JsExecutionException;
import org.apache.log4j.Logger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class JSExecutor implements Executor {
    private static final Logger LOGGER = Logger.getLogger(JSExecutor.class);

    private final ScriptEngine engine;

    public JSExecutor() {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        this.engine = scriptEngineManager.getEngineByName("JavaScript");
    }

    @Override
    public void execute(String code) {
        try {
            Object result = engine.eval(code);
            LOGGER.info("Execution result: " + result);
        } catch (ScriptException e) {
            throw new JsExecutionException();
        }
    }
}
