package com.brainfuck.transpiler.executors;

public interface Executor {
    void execute(String code);
}
